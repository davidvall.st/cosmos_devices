import time
import RPi.GPIO as GPIO

butPin = 17 # Broadcom pin 17 (P1 pin 11)

GPIO.setmode(GPIO.BCM) # Broadcom pin-numbering scheme
GPIO.setup(butPin, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Button pin set as input w/ pull-up

count = 0

def my_callback(channel):
    global count
    count = count + 1
    #print('Edge detected on channel %s'%channel)


GPIO.add_event_detect(butPin, GPIO.RISING, callback=my_callback)  # add rising edge detection on a channel

while 1:
    time.sleep(1) 
    print('Count',count)
    count = 0
