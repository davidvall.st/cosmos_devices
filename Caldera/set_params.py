import pickle
import os

params = {
    'flow_water_acum' : 116.878
}

def save_params():
	with open('params.pickle', 'wb') as f:
		pickle.dump(params, f)


save_params()
