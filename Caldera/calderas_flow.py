import time
import RPi.GPIO as GPIO


#!/usr/bin/python
# -*- coding: latin-1 -*-
import time
import uuid
from azure.iot.device import IoTHubDeviceClient, Message
from threading import Thread
import json
import sys
import signal
from datetime import datetime, tzinfo, timedelta, timezone
from dateutil.tz import tzlocal
import spidev
import RPi.GPIO as GPIO

CONNECTION_STRING = "HostName=iothub-czthu.azure-devices.net;DeviceId=CyD_Caldera;SharedAccessKey=7ymaRB0Y/ePU26dM9e96M1SrlimyH9bAuX4c3yItVB0="

hosts = ['caldera']
md =['caldera']
nummd =[1010]
w=23
h=8
data = [[0 for x in range(w)] for y in range(h)] 
ts = ""
temp = 0

#Flow meter conf
flow_meter_pin = 17 # Broadcom pin 17 (P1 pin 11)

GPIO.setmode(GPIO.BCM) # Broadcom pin-numbering scheme
GPIO.setup(flow_meter_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Button pin set as input$

count_pulses = 0
flow_water = 0
flow_water_acum = 0

def flow_meter_callback(channel):
    global count_pulses
    count_pulses = count_pulses + 1
    #print('Edge detected on channel %s'%channel)

GPIO.add_event_detect(flow_meter_pin, GPIO.RISING, callback=flow_meter_callback)  # add rising $

class simple_utc(tzinfo):
    def tzname(self,**kwargs):
        return "UTC"
    def utcoffset(self, dt):
        return timedelta(0)

def iothub_client_init():
    # Create an IoT Hub client
    client = IoTHubDeviceClient.create_from_connection_string(CONNECTION_STRING)
    return client


def iothub_sender():

    try:
        client = iothub_client_init()
        reported_properties = {
                                "Longitude": -76.541882,
                                "Location": "Rear Door",
                                "Type": "CALDERA",
                                "Latitude": 3.437625,
                                "Model": "CARNICOS",
                                "Firmware": "1.0",
                                "Telemetry": {
                                    "analyzer;v1": {
                                        "Interval": "00:00:01",
                                        "MessageTemplate": "{\"HwId\":${HwId},\"NumId\":${NumId},\"temperature\":${temperature},\"flow_water_acum\":${flow_water_acum},\"flow_water\":${flow_water}}",
                                        "MessageSchema": {
                                            "Name": "caldera;v1",
                                            "Format": "JSON",
                                            "Fields": {
                                            "HwId": "Text",
                                            "NumId" : "Double",
                                            "temperature": "Double",
                                            "flow_water": "Double",
                                            "flow_water_acum": "Double",
                                            }
                                        }
                                    }
                                }
                            }
        client.patch_twin_reported_properties(reported_properties)
        print("IoT Hub device sending periodic messages, press Ctrl-C to exit")
        msgs=[]
        global temp,flow_water,flow_water_acum
        while True:
            if len(data) > 0:
                #print(data)
                ts=datetime.utcnow().replace(tzinfo=simple_utc()).isoformat()
                ts = str(ts).replace('+00:00', 'Z')
                print(ts)
                msg= {
                        "HwId": md[0],
                        "NumId": nummd[0],
                        "ts": ts,
                        "temperature": temp,
                        "flow_water": flow_water,
                        "flow_water_acum": flow_water_acum,
                }
                message = Message(json.dumps(msg))
                message.message_id= uuid.uuid4()
                message.content_encoding = 'utf-8'
                message.content_type = 'application/json'
                message.custom_properties["$$MessageSchema"] = 'analyzer;v1'
                message.custom_properties["$$ContentType"] = 'JSON'
                client.send_message(message)

                print("Message successfully sent")
                msgs = []
                time.sleep(30)
    except KeyboardInterrupt:
        print("IoTHubClient stopped")

def safe_run(func):

    def func_wrapper(*args, **kwargs):

        try:
           return func(*args, **kwargs)

        except Exception as e:
            print(e)
            return None

    return func_wrapper



def spi_aquisition(): 
    global temp
    spi = spidev.SpiDev()
    spi.open(0, 0)
    spi.max_speed_hz = 3900000
    while True:
        t = spi.readbytes(2)
        time.sleep(5)

        msb = format(t[0], '#010b')
        lsb = format(t[1], '#010b')

        r_temp = msb[2:] + lsb[2:]
        t_bytes = "0b" + r_temp[0:13]
        temp = int(t_bytes, base=2)*0.25
        print('Temperature:',temp)

def flow_meter_adquisition():
	global count_pulses,flow_water,flow_water_acum
	while True:
	    time.sleep(1) 
	    print('Count_pulses',count_pulses)
	    flow_water = count_pulses
	    flow_water_acum = flow_water_acum + flow_water
	    count_pulses = 0

if __name__ == "__main__":

    
    threads = []
    threads.append(Thread(target = iothub_sender))
    threads.append(Thread(target = spi_aquisition))
    threads.append(Thread(target = flow_meter_adquisition))
    try:
        for t in threads:
            t.setDaemon(True)
            t.start()
            t.join(1)
        signal.pause()
    except (KeyboardInterrupt, SystemExit):
        print ("! Received keyboard interrupt, quitting threads.")
        for t in threads:
            t.kill_received = True
        sys.exit()

