#!/usr/bin/python
# -*- coding: latin-1 -*-
import git
import sentry_sdk
import struct
import logging
from pymodbus.client.sync import ModbusTcpClient
import time
import uuid
from azure.iot.device import IoTHubDeviceClient, Message, MethodResponse
from threading import Thread
import json
import sys
import signal
from datetime import datetime, tzinfo, timedelta, timezone
from dateutil.tz import tzlocal

CONNECTION_STRINGS = [  'HostName=iothub-czthu.azure-devices.net;DeviceId=CyD_Taller;SharedAccessKey=AyHyE4sSshBKIhY1taKRkenoL43OXBuOHeOHs0x9wTw=',
                        'HostName=iothub-czthu.azure-devices.net;DeviceId=Bovinos;SharedAccessKey=K3J2fHFkexlmg3JFv39YAfMicmrBR0sw1AwDSde2b3w=',
                        'HostName=iothub-czthu.azure-devices.net;DeviceId=Porcinos;SharedAccessKey=91ECSQtk5EgNGjOXlB0PIc2c/SKPpmeFgeUrrDgqJIk=',
                        'HostName=iothub-czthu.azure-devices.net;DeviceId=CF_24_33;SharedAccessKey=HFDxJRQQavmpP8WfMl4p+q0IkujmrhdolOFpnStgs7U=',
                        'HostName=iothub-czthu.azure-devices.net;DeviceId=CF_14_15;SharedAccessKey=B6M2weKVVBD1Kxw/a/yaZ+PV7AqJSpEv6l3O6gcIePA=',
                        'HostName=iothub-czthu.azure-devices.net;DeviceId=CF_2_PISO;SharedAccessKey=2K7l30FflEbY6oVo7oNb2wbuJFgFOqFeX84ixEwC5oU=',
                        'HostName=iothub-czthu.azure-devices.net;DeviceId=CF_1_2;SharedAccessKey=UqvcZKPKc8oMIZGCKjYoIHfvca0knytIziGLtinvEiI=',
                        'HostName=iothub-czthu.azure-devices.net;DeviceId=Subproductos;SharedAccessKey=KY8hnc8ZqZ66inOQYGbjZBirGpEROdzyMJBVPsEfVn8='
                    ]


md =['bovinos_pac3220','porcinos_pac3220','CF_24_33_pac3220','CF_14_15_pac3220','CF_2_PISO_pac3220','CF_1_2_pac3223','subproductos_pac3220']
nummd =[1001,1002,1003,1004,1005,1006,1008]
w=31
h=8
data = [[0 for x in range(w)] for y in range(h)] 
ts = ""
firts_run = 1
INTERVAL = 1

def restart():
    command = "/usr/bin/sudo /sbin/shutdown -r now"
    import subprocess
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print (output)

def twin_reported_properties(client):
    global INTERVAL
    reported_properties = {
                        "Location": "Taller",
                        "Longitude": -76.411002,
                        "Latitude": 3.4145113,
                        "Type": "Analyzer",
                        "TelemetryInterval": INTERVAL,
                        "Model": "PAC3220",
                        "Firmware": "1.0",
                        "SupportedMethods": "Restart,UpdateFirmware",
                        "Telemetry": {
                            "analyzer;v1": {
                                "Interval": "00:00:01",
                                "MessageTemplate": "{\"HwId\":${HwId},\"NumId\":${NumId},\"voltageL1N\":${voltageL1N},\"currentL2\":${currentL2},\"activePowerL1\":${activePowerL1}}",
                                "MessageSchema": {
                                    "Name": "analyzer;v1",
                                    "Format": "JSON",
                                    "Fields": {
                                    "HwId": "Text",
                                    "NumId" : "Double",
                                    "voltageL1N": "Double",
                                    "currentL2": "Double",
                                    "activePowerL1" : "Double"
                                    }
                                }
                            }
                        }
                    }
    client.patch_twin_reported_properties(reported_properties)

def twin_update_listener(client):
    global INTERVAL
    while True:
        patch = client.receive_twin_desired_properties_patch()  # blocking call
        print("Twin patch received:")
        print(patch)
        try:
            INTERVAL = patch['TelemetryInterval']
            twin_reported_properties(client)
        except ValueError as ve:
            print(ve)
        
        

def device_method_listener(client):
    global INTERVAL
    while True:
        method_request = client.receive_method_request()
        print (
            "\nMethod callback called with:\nmethodName = {method_name}\npayload = {payload}".format(
                method_name=method_request.name,
                payload=method_request.payload
            )
        )
        if method_request.name == "Restart":
            try:
                reset_count = 1
            except ValueError:
                response_payload = {"Response": "Invalid parameter"}
                response_status = 404
            else:
                response_payload = {"Response": "Executed direct method {}".format(method_request.name)}
                response_status = 200
                method_response = MethodResponse(method_request.request_id, response_status, payload=response_payload)
                client.send_method_response(method_response)
                restart()
        elif method_request.name == "UpdateFirmware":
            try:
                print("updating...")
                g = git.Git('/home/pi/Projects/cosmos_devices/')
                g.pull('origin','develop')
                print("updated...")
                print("Restarting...")
                
            except ValueError:
                response_payload = {"Response": "Invalid parameter"}
                response_status = 404
            else:
                response_payload = {"Response": "Executed direct method {}".format(method_request.name)}
                response_status = 200
                method_response = MethodResponse(method_request.request_id, response_status, payload=response_payload)
                client.send_method_response(method_response)
                restart()
        else:
            response_payload = {"Response": "Direct method {} not defined".format(method_request.name)}
            method_response = MethodResponse(method_request.request_id, response_status, payload=response_payload)
            client.send_method_response(method_response)

class simple_utc(tzinfo):
    def tzname(self,**kwargs):
        return "UTC"
    def utcoffset(self, dt):
        return timedelta(0)

def safe_run(func):

    def func_wrapper(*args, **kwargs):

        try:
           return func(*args, **kwargs)

        except Exception as e:
            print(e)
            return None

    return func_wrapper

def iothub_client_init():
    # Create an IoT Hub client
    client= []
    global INTERVAL
    for x in range(len(CONNECTION_STRINGS)):
        client.append(IoTHubDeviceClient.create_from_connection_string(CONNECTION_STRINGS[x]))
        #print(client)
        # Start a thread to listen 
    desired=client[0].get_twin()
    INTERVAL=desired['desired']['TelemetryInterval']
    for x in range(len(CONNECTION_STRINGS)):
        twin_reported_properties(client[x])
    print(desired['desired']['TelemetryInterval'])
    return client

@safe_run
def modbus_to_float(modclient,register,len_bytes):
    result=modclient.read_holding_registers(register, len_bytes, unit = 1)
    value = struct.pack('>I',(result.registers[0]<<16)|result.registers[1]) #4 bytes concatenation
    valor_float = struct.unpack('!f', value)[0] #to float
    return valor_float

def iothub_sender():
    global INTERVAL
    try:
        client = iothub_client_init()
        print(client)
        device_method_thread = Thread(target=device_method_listener, args=(client[0],))
        device_method_thread.daemon = True
        device_method_thread.start()
        device_method_thread_desired = Thread(target=twin_update_listener, args=(client[0],))
        device_method_thread_desired.daemon = True
        device_method_thread_desired.start()
        print("IoT Hub device sending periodic messages, press Ctrl-C to exit")
        msgs=[]
        global firts_run
        while True:
            if len(data) > 0:
                if(firts_run == 0):
                    #print(data)
                    ts=datetime.utcnow().replace(tzinfo=simple_utc()).isoformat()
                    ts = str(ts).replace('+00:00', 'Z')
                    print(ts)
                    
                    for x in hosts:
                        hw=md[hosts.index(x)]
                        hwn=nummd[hosts.index(x)]
                        msg= {
                                "HwId": hw,
                                "NumId": hwn,
                                "ts": ts,
                                "status": "connected",
                                "voltageL1N": data[hosts.index(x)][0],
                                "voltageL2N": data[hosts.index(x)][1],
                                "voltageL3N": data[hosts.index(x)][2],
                                "voltageL1L2":  data[hosts.index(x)][3],
                                "voltageL2L3":  data[hosts.index(x)][4],
                                "voltageL3L1":  data[hosts.index(x)][5],
                                "currentL1":    data[hosts.index(x)][6],
                                "currentL2":   data[hosts.index(x)][7], 
                                "currentL3":   data[hosts.index(x)][8], 
                                "activePowerL1":   data[hosts.index(x)][9], 
                                "activePowerL2":   data[hosts.index(x)][10],
                                "activePowerL3":   data[hosts.index(x)][11], 
                                "reactivePowerL1":   data[hosts.index(x)][12], 
                                "reactivePowerL2":   data[hosts.index(x)][13], 
                                "reactivePowerL3":   data[hosts.index(x)][14], 
                                "powerFactorL1":   data[hosts.index(x)][15], 
                                "powerFactorL2":   data[hosts.index(x)][16], 
                                "powerFactorL3":   data[hosts.index(x)][17], 
                                "frequency":   data[hosts.index(x)][18], 
                                "totalApparentPower":   data[hosts.index(x)][19], 
                                "totalActivePower":   data[hosts.index(x)][20], 
                                "totalReactivePower":   data[hosts.index(x)][21], 
                                "totalPowerFactor":   data[hosts.index(x)][22], 
                                "currentN":   data[hosts.index(x)][23],
                                "cumulatedAvergaeActivePowerImport":   data[hosts.index(x)][24], 
                                "cumulatedAvergaeReactivePowerImport":   data[hosts.index(x)][25], 
                                "cumulatedAvergaeActivePowerExport":   data[hosts.index(x)][26], 
                                "cumulatedAvergaeRectivePowerExport":   data[hosts.index(x)][27],
                                "totalActiveEnergyImportTariff1":   data[hosts.index(x)][28], 
                                "totalActiveEnergyImportTariff2":   data[hosts.index(x)][29]
                        }
                        message = Message(json.dumps(msg))
                        message.message_id= uuid.uuid4()
                        message.content_encoding = 'utf-8'
                        message.content_type = 'application/json'
                        message.custom_properties["$$MessageSchema"] = 'analyzer;v1'
                        message.custom_properties["$$ContentType"] = 'JSON'
                        client[0].send_message(message)
                        print("Sending message: {}".format(message))
                    for x in range(len(CONNECTION_STRINGS)):
                        msg= {
                                "HwId": "null",
                                "NumId": 0,
                                "ts": ts,
                                "status": "connected",
                                "voltageL1N": data[x][0],
                                "voltageL2N": data[x][1],
                                "voltageL3N": data[x][2],
                                "voltageL1L2":  data[x][3],
                                "voltageL2L3":  data[x][4],
                                "voltageL3L1":  data[x][5],
                                "currentL1":    data[x][6],
                                "currentL2":   data[x][7], 
                                "currentL3":   data[x][8], 
                                "activePowerL1":   data[x][9], 
                                "activePowerL2":   data[x][10],
                                "activePowerL3":   data[x][11], 
                                "reactivePowerL1":   data[x][12], 
                                "reactivePowerL2":   data[x][13], 
                                "reactivePowerL3":   data[x][14], 
                                "powerFactorL1":   data[x][15], 
                                "powerFactorL2":   data[x][16], 
                                "powerFactorL3":   data[x][17], 
                                "frequency":   data[x][18], 
                                "totalApparentPower":   data[x][19], 
                                "totalActivePower":   data[x][20], 
                                "totalReactivePower":   data[x][21], 
                                "totalPowerFactor":   data[x][22], 
                                "currentN":   data[x][23],
                                "cumulatedAvergaeActivePowerImport":   data[x][24], 
                                "cumulatedAvergaeReactivePowerImport":   data[x][25], 
                                "cumulatedAvergaeActivePowerExport":   data[x][26], 
                                "cumulatedAvergaeRectivePowerExport":   data[x][27],
                                "totalActiveEnergyImportTariff1":   data[x][28], 
                                "totalActiveEnergyImportTariff2":   data[x][29]
                        }
                        message = Message(json.dumps(msg))
                        message.message_id= uuid.uuid4()
                        message.content_encoding = 'utf-8'
                        message.content_type = 'application/json'
                        message.custom_properties["$$MessageSchema"] = 'analyzer;v1'
                        message.custom_properties["$$ContentType"] = 'JSON'
                        if x <= 6:
                            client[x+1].send_message(message)
                
                print("Message successfully sent")
                msgs = []
                time.sleep(INTERVAL)
    except KeyboardInterrupt:
        print("IoTHubClient stopped")



@safe_run
def modbus_aquisition(hosts, registers,len_bytes): 
    global firts_run
    while True:
        for x in hosts:
            modclient = ModbusTcpClient(hosts[hosts.index(x)], port=502, timeout=3)
            res = modclient.connect()
            if res:
                for i in range(len(registers)):
                    data[hosts.index(x)][i]= modbus_to_float(modclient,registers[i],len_bytes[i]) 
                    time.sleep(0.2)
                modclient.close()
                time.sleep(0.5)
            
            else:
                print(datetime.now(tzlocal()),"Modbus TCP/IP connection failed:",x, sep="---")
        print ("All devices recorded")
        firts_run = 0
            
    
if __name__ == "__main__":

    sentry_sdk.init(
    "https://d26cdbd2e623402d9ff2799acb549d27@o456666.ingest.sentry.io/5449980",
    traces_sample_rate=1.0
    )
    hosts = ['169.254.93.246', '169.254.93.247','169.254.93.248','169.254.93.249','169.254.93.250','169.254.93.251','169.254.93.253']
    registers = [1,3,5,7,9,11,13,15,17,25,27,29,31,33,35,37,39,41,55,63,65,67,69,223,501,503,505,507,801,805,231]
    len_bytes = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,4,2]
    threads = []
    threads.append(Thread(target = iothub_sender))
    threads.append(Thread(target = modbus_aquisition, args = (hosts,registers,len_bytes)))
    try:
        for t in threads:
            t.setDaemon(True)
            t.start()
            t.join(1)
        signal.pause()
    except (KeyboardInterrupt, SystemExit, Exception):
        print ("! Received keyboard interrupt, quitting threads, exiting...")
        for t in threads:
            t.kill_received = True
        sys.exit()

    