# Copyright (c) Microsoft. All rights reserved.
# Licensed under the MIT license. See LICENSE file in the project root for full license information.
import git
import time
import uuid
from threading import Thread
import pprint
import json
# Using the Python Device SDK for IoT Hub:
#   https://github.com/Azure/azure-iot-sdk-python
# The sample connects to a device-specific MQTT endpoint on your IoT Hub.
from azure.iot.device import IoTHubDeviceClient, Message, MethodResponse

# The device connection string to authenticate the device with your IoT hub.
# Using the Azure CLI:
# az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
CONNECTION_STRING = "HostName=iothub-czthu.azure-devices.net;DeviceId=test;SharedAccessKey=KAx1wApynqpzb1OsKtylBl56VesrvSkBceMlKQe+Muo="
jj= {'Location': 'Rear Door', 'Longitude': -76.541882, 'Latitude': 3.437623, 'Type': 'Analyzer', 'TelemetryInterval': 1, 'Model': 'PAC3220', '$version': 4}

# Define the JSON message to send to IoT Hub.
INTERVAL = 1
TEMPERATURE = 20.0
HUMIDITY = 60
MSG_TXT = [ '{"currentN": 0.07133208215236664, "voltageL1L2": 478.10943603515625, "activePowerL1": 2669.64892578125, "powerFactorL1": 0.4334031641483307, "totalActiveEnergyImportTariff2": 0.0, "currentL2": 25.010623931884766, "frequency": 60.007450103759766, "totalPowerFactor": 0.3779056668281555, "voltageL3L1": 474.8887023925781, "currentL1": 22.91274070739746, "cumulatedAvergaeRectivePowerExport": 0.0, "reactivePowerL3": 5947.10498046875, "reactivePowerL1": 5684.4833984375, "totalApparentPower": 19554.8359375, "totalActivePower": 7397.3095703125, "activePowerL2": 2551.044189453125, "cumulatedAvergaeActivePowerExport": 0.0, "totalReactivePower": 18118.890625, "totalActiveEnergyImportTariff1": 9.049850463867188, "ts": "2020-10-15T22:32:38.268634Z", "activePowerL3": 2074.771484375, "voltageL2L3": 479.5567321777344, "currentL3": 22.926029205322266, "cumulatedAvergaeReactivePowerImport": 15897.7841796875, "powerFactorL2": 0.3821727931499481, "NumId": 1001, "HwId": "bovinos_pac3220", "reactivePowerL2": 6428.9091796875, "voltageL3N": 275.50262451171875, "cumulatedAvergaeActivePowerImport": 5440.45068359375, "voltageL1N": 274.8102111816406, "voltageL2N": 277.0617370605469, "powerFactorL3": 0.34101051092147827}']

def restart():
    command = "/usr/bin/sudo /sbin/shutdown -r now"
    import subprocess
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output = process.communicate()[0]
    print (output)


def twin_reported_propertiesr(client):
    global INTERVAL
    reported_properties = {
                                "Location": "Rear Door",
                                "Longitude": -76.541882,
                                "Latitude": 3.437623,
                                "Type": "Analyzer",
                                "TelemetryInterval": INTERVAL,
                                "Model": "PAC3220",
                                "Firmware": "1.0",
                                "SupportedMethods": "Restart,UpdateFirmware",
                                "Telemetry": {
                                    "analyzer;v1": {
                                        "Interval": "00:00:01",
                                        "MessageTemplate": "{\"HwId\":${HwId},\"NumId\":${NumId},\"voltageL1N\":${voltageL1N},\"currentL2\":${currentL2},\"activePowerL1\":${activePowerL1}}, \"activePowerL2\":${activePowerL2}}",
                                        "MessageSchema": {
                                            "Name": "analyzer;v1",
                                            "Format": "JSON",
                                            "Fields": {
                                            "HwId": "Text",
                                            "NumId" : "Double",
                                            "voltageL1N": "Double",
                                            "currentL2": "Double",
                                            "activePowerL1" : "Double",
                                            "activePowerL2" : "Double"
                                            }
                                        }
                                    }
                                }
                            }
    client.patch_twin_reported_properties(reported_properties)

def twin_update_listener(client):
    global INTERVAL
    while True:
        patch = client.receive_twin_desired_properties_patch()  # blocking call
        print("Twin patch received:")
        print(patch)
        try:
            INTERVAL = patch['TelemetryInterval']
            twin_reported_propertiesr(client)
        except ValueError as ve:
            print(ve)
        
        

def device_method_listener(client):
    global INTERVAL
    while True:
        method_request = client.receive_method_request()
        print (
            "\nMethod callback called with:\nmethodName = {method_name}\npayload = {payload}".format(
                method_name=method_request.name,
                payload=method_request.payload
            )
        )
        if method_request.name == "Restart":
            try:
                reset_count = 1
            except ValueError:
                response_payload = {"Response": "Invalid parameter"}
                response_status = 404
            else:
                response_payload = {"Response": "Executed direct method {}".format(method_request.name)}
                response_status = 200
                method_response = MethodResponse(method_request.request_id, response_status, payload=response_payload)
                client.send_method_response(method_response)
                restart()
        elif method_request.name == "UpdateFirmware":
            try:
                print("updating...")
                g = git.Git('/home/pi/Projects/cosmos_devices/')
                g.pull('origin','develop')
                print("updated...")
                print("Restarting...")
                
            except ValueError:
                response_payload = {"Response": "Invalid parameter"}
                response_status = 404
            else:
                response_payload = {"Response": "Executed direct method {}".format(method_request.name)}
                response_status = 200
                method_response = MethodResponse(method_request.request_id, response_status, payload=response_payload)
                client.send_method_response(method_response)
                restart()
        else:
            response_payload = {"Response": "Direct method {} not defined".format(method_request.name)}
            method_response = MethodResponse(method_request.request_id, response_status, payload=response_payload)
            client.send_method_response(method_response)

def iothub_client_init():
    # Create an IoT Hub client
    global INTERVAL
    client = IoTHubDeviceClient.create_from_connection_string(

        CONNECTION_STRING)
        # Start a thread to listen 
    desired=client.get_twin()
    INTERVAL=desired['desired']['TelemetryInterval']
    twin_reported_propertiesr(client)
   
    print(desired['desired']['TelemetryInterval'])
    print("\nHOLA3")
    return client


def iothub_client_telemetry_sample_run():

    try:
        client = iothub_client_init()
        device_method_thread = Thread(target=device_method_listener, args=(client,))
        device_method_thread.daemon = True
        device_method_thread.start()
        device_method_thread_desired = Thread(target=twin_update_listener, args=(client,))
        device_method_thread_desired.daemon = True
        device_method_thread_desired.start()
        print("IoT Hub device sending periodic messages, press Ctrl-C to exit")

        while True:
            # Build the message with simulated telemetry values.
            for x in MSG_TXT:
                message = Message(MSG_TXT[MSG_TXT.index(x)])
                message.message_id= uuid.uuid4()
                message.content_encoding = 'utf-8'
                message.content_type = 'application/json'
                print("Sending message: {}".format(message))
                client.send_message(message)
            print("Message successfully sent")
            time.sleep(INTERVAL)

    except KeyboardInterrupt:
        print("IoTHubClient sample stopped")


if __name__ == '__main__':
    print("IoT Hub Quickstart #1 - Simulated device")
    print("Press Ctrl-C to exit")
    iothub_client_telemetry_sample_run()
